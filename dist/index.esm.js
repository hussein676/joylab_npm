import React, { forwardRef, useRef, useState, useEffect, useImperativeHandle } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import axios from 'axios';
import '../src/components/Dropzone.css';
import '../src/components/fontawesome/css/all.css';

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) {
  try {
    var info = gen[key](arg);
    var value = info.value;
  } catch (error) {
    reject(error);
    return;
  }

  if (info.done) {
    resolve(value);
  } else {
    Promise.resolve(value).then(_next, _throw);
  }
}

function _asyncToGenerator(fn) {
  return function () {
    var self = this,
        args = arguments;
    return new Promise(function (resolve, reject) {
      var gen = fn.apply(self, args);

      function _next(value) {
        asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value);
      }

      function _throw(err) {
        asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err);
      }

      _next(undefined);
    });
  };
}

function _slicedToArray(arr, i) {
  return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest();
}

function _toConsumableArray(arr) {
  return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread();
}

function _arrayWithoutHoles(arr) {
  if (Array.isArray(arr)) return _arrayLikeToArray(arr);
}

function _arrayWithHoles(arr) {
  if (Array.isArray(arr)) return arr;
}

function _iterableToArray(iter) {
  if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter);
}

function _iterableToArrayLimit(arr, i) {
  if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return;
  var _arr = [];
  var _n = true;
  var _d = false;
  var _e = undefined;

  try {
    for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) {
      _arr.push(_s.value);

      if (i && _arr.length === i) break;
    }
  } catch (err) {
    _d = true;
    _e = err;
  } finally {
    try {
      if (!_n && _i["return"] != null) _i["return"]();
    } finally {
      if (_d) throw _e;
    }
  }

  return _arr;
}

function _unsupportedIterableToArray(o, minLen) {
  if (!o) return;
  if (typeof o === "string") return _arrayLikeToArray(o, minLen);
  var n = Object.prototype.toString.call(o).slice(8, -1);
  if (n === "Object" && o.constructor) n = o.constructor.name;
  if (n === "Map" || n === "Set") return Array.from(o);
  if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen);
}

function _arrayLikeToArray(arr, len) {
  if (len == null || len > arr.length) len = arr.length;

  for (var i = 0, arr2 = new Array(len); i < len; i++) arr2[i] = arr[i];

  return arr2;
}

function _nonIterableSpread() {
  throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
}

function _nonIterableRest() {
  throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
}

var Dropzone = /*#__PURE__*/forwardRef(function (props, ref) {
  var fileInputRef = useRef();
  var modalImageRef = useRef();
  var modalRef = useRef();
  var progressRef = useRef();
  var uploadRef = useRef();
  var uploadModalRef = useRef();

  var _useState = useState([]),
      _useState2 = _slicedToArray(_useState, 2),
      selectedFiles = _useState2[0],
      setSelectedFiles = _useState2[1];

  var _useState3 = useState([]),
      _useState4 = _slicedToArray(_useState3, 2),
      validFiles = _useState4[0],
      setValidFiles = _useState4[1];

  var _useState5 = useState([]),
      _useState6 = _slicedToArray(_useState5, 2),
      unsupportedFiles = _useState6[0],
      setUnsupportedFiles = _useState6[1];

  var _useState7 = useState(''),
      _useState8 = _slicedToArray(_useState7, 2),
      errorMessage = _useState8[0],
      setErrorMessage = _useState8[1];

  useEffect(function () {
    var filteredArr = selectedFiles.reduce(function (acc, current) {
      var x = acc.find(function (item) {
        return item.name === current.name;
      });

      if (!x) {
        return acc.concat([current]);
      } else {
        return acc;
      }
    }, []);
    setValidFiles(_toConsumableArray(filteredArr));
  }, [selectedFiles]);
  useImperativeHandle(ref, function () {
    return {
      remove: function remove() {}
    };
  });

  var preventDefault = function preventDefault(e) {
    e.preventDefault(); // e.stopPropagation();
  };

  var dragOver = function dragOver(e) {
    preventDefault(e);
  };

  var dragEnter = function dragEnter(e) {
    preventDefault(e);
  };

  var dragLeave = function dragLeave(e) {
    preventDefault(e);
  };

  var fileDrop = function fileDrop(e) {
    preventDefault(e);
    var files = e.dataTransfer.files;

    if (files.length) {
      handleFiles(files);
    }
  };

  var filesSelected = function filesSelected() {
    if (fileInputRef.current.files.length) {
      handleFiles(fileInputRef.current.files);
    }
  };

  var fileInputClicked = function fileInputClicked() {
    fileInputRef.current.click();
  };

  var handleFiles = function handleFiles(files) {
    if (files[0].size / 1024 > 2000) {
      props.getData(null);
      files[0]['invalid'] = true;
      setSelectedFiles(function (prevArray) {
        return [files[0]];
      });
      setErrorMessage('File size larger than 2MB');
      setUnsupportedFiles(function (prevArray) {
        return [files[0]];
      });
      return;
    }

    if (validateFile(files[0])) {
      setSelectedFiles(function (prevArray) {
        return [files[0]];
      });
      props.getData(files[0]);
    } else {
      props.getData(null);
      files[0]['invalid'] = true;
      setSelectedFiles(function (prevArray) {
        return [files[0]];
      });
      setErrorMessage('File type not permitted ');
      setUnsupportedFiles(function (prevArray) {
        return [files[0]];
      });
    }
  };

  var validateFile = function validateFile(file) {
    var validTypes = ['text/csv', 'csv', 'xlsx', 'xlsm'];
    var extension = file.name.split(".");

    if (validTypes.indexOf(file.type) === -1 && validTypes.indexOf(extension[1]) === -1) {
      return false;
    }

    return true;
  };

  var fileSize = function fileSize(size) {
    if (size === 0) {
      return '0 Bytes';
    }

    var k = 1024;
    var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
    var i = Math.floor(Math.log(size) / Math.log(k));
    return parseFloat((size / Math.pow(k, i)).toFixed(2)) + ' ' + sizes[i];
  };

  var fileType = function fileType(fileName) {
    return fileName.substring(fileName.lastIndexOf('.') + 1, fileName.length) || fileName;
  };

  var removeFile = function removeFile(name) {
    props.getData(null);
    var index = validFiles.findIndex(function (e) {
      return e.name === name;
    });
    var index2 = selectedFiles.findIndex(function (e) {
      return e.name === name;
    });
    var index3 = unsupportedFiles.findIndex(function (e) {
      return e.name === name;
    });
    validFiles.splice(index, 1);
    selectedFiles.splice(index2, 1);
    setValidFiles(_toConsumableArray(validFiles));
    setSelectedFiles(_toConsumableArray(selectedFiles));

    if (index3 !== -1) {
      unsupportedFiles.splice(index3, 1);
      setUnsupportedFiles(_toConsumableArray(unsupportedFiles));
    }
  };

  var openImageModal = function openImageModal(file) {
    return;
    /*
    const reader = new FileReader();
    modalRef.current.style.display = "block";
    reader.readAsDataURL(file);
    reader.onload = function(e) {
        modalImageRef.current.style.backgroundImage = `url(${e.target.result})`;
    }
    */
  };

  var closeModal = function closeModal() {
    modalRef.current.style.display = "none";
    modalImageRef.current.style.backgroundImage = 'none';
  };
  /*
      const uploadFiles = async () => {
          uploadModalRef.current.style.display = 'block';
          uploadRef.current.innerHTML = 'File(s) Uploading...';
          for (let i = 0; i < validFiles.length; i++) {
              const formData = new FormData();
              formData.append('image', validFiles[i]);
              formData.append('key', '');
  
              axios.post('https://api.imgbb.com/1/upload', formData, {
                  onUploadProgress: (progressEvent) => {
                      const uploadPercentage = Math.floor((progressEvent.loaded / progressEvent.total) * 100);
                      progressRef.current.innerHTML = `${uploadPercentage}%`;
                      progressRef.current.style.width = `${uploadPercentage}%`;
  
                      if (uploadPercentage === 100) {
                          uploadRef.current.innerHTML = 'File(s) Uploaded';
                          validFiles.length = 0;
                          setValidFiles([...validFiles]);
                          setSelectedFiles([...validFiles]);
                          setUnsupportedFiles([...validFiles]);
                      }
                  },
              })
              .catch(() => {
                  uploadRef.current.innerHTML = `<span class="error">Error Uploading File(s)</span>`;
                  progressRef.current.style.backgroundColor = 'red';
              })
          }
      }
      */


  var closeUploadModal = function closeUploadModal() {
    uploadModalRef.current.style.display = 'none';
  };

  return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("div", {
    className: "container"
  }, /*#__PURE__*/React.createElement("div", {
    className: "drop-container",
    onDragOver: dragOver,
    onDragEnter: dragEnter,
    onDragLeave: dragLeave,
    onDrop: fileDrop,
    onClick: fileInputClicked
  }, /*#__PURE__*/React.createElement("div", {
    className: "drop-message"
  }, /*#__PURE__*/React.createElement("div", {
    className: "upload-icon"
  }), "Drag & Drop files here or click to select file(s)"), /*#__PURE__*/React.createElement("input", {
    ref: fileInputRef,
    className: "file-input",
    type: "file",
    multiple: true,
    onChange: filesSelected
  })), /*#__PURE__*/React.createElement("div", {
    className: "file-display-container"
  }, validFiles.map(function (data, i) {
    return /*#__PURE__*/React.createElement("div", {
      className: "file-status-bar",
      key: i
    }, /*#__PURE__*/React.createElement("div", {
      onClick: !data.invalid ? function () {
        return openImageModal();
      } : function () {
        return removeFile(data.name);
      }
    }, /*#__PURE__*/React.createElement("div", {
      className: "file-type-logo"
    }), /*#__PURE__*/React.createElement("div", {
      className: "file-type"
    }, fileType(data.name)), /*#__PURE__*/React.createElement("span", {
      className: "file-name ".concat(data.invalid ? 'file-error' : '')
    }, data.name), /*#__PURE__*/React.createElement("span", {
      className: "file-size"
    }, "(", fileSize(data.size), ")"), " ", data.invalid && /*#__PURE__*/React.createElement("span", {
      className: "file-error-message"
    }, "(", errorMessage, ")")), /*#__PURE__*/React.createElement("div", {
      className: "file-remove",
      onClick: function onClick() {
        return removeFile(data.name);
      }
    }, "X"));
  }))), /*#__PURE__*/React.createElement("div", {
    className: "modal",
    ref: modalRef
  }, /*#__PURE__*/React.createElement("div", {
    className: "overlay"
  }), /*#__PURE__*/React.createElement("span", {
    className: "close",
    onClick: function onClick() {
      return closeModal();
    }
  }, "X"), /*#__PURE__*/React.createElement("div", {
    className: "modal-image",
    ref: modalImageRef
  })), /*#__PURE__*/React.createElement("div", {
    className: "upload-modal",
    ref: uploadModalRef
  }, /*#__PURE__*/React.createElement("div", {
    className: "overlay"
  }), /*#__PURE__*/React.createElement("div", {
    className: "close",
    onClick: function onClick() {
      return closeUploadModal();
    }
  }, "X"), /*#__PURE__*/React.createElement("div", {
    className: "progress-container"
  }, /*#__PURE__*/React.createElement("span", {
    ref: uploadRef
  }), /*#__PURE__*/React.createElement("div", {
    className: "progress"
  }, /*#__PURE__*/React.createElement("div", {
    className: "progress-bar",
    ref: progressRef
  })))));
});

function FileUploadForm() {
  var DropzoneRef = useRef();

  var _useState = useState([]),
      _useState2 = _slicedToArray(_useState, 2),
      rowdata = _useState2[0],
      setRowdata = _useState2[1];

  var _useState3 = useState([]),
      _useState4 = _slicedToArray(_useState3, 2),
      sugg_header = _useState4[0],
      setSuggHeader = _useState4[1];

  var _useState5 = useState([]),
      _useState6 = _slicedToArray(_useState5, 2),
      sugg_mult_header = _useState6[0],
      setSuggMultHeader = _useState6[1];

  var _useState7 = useState([]),
      _useState8 = _slicedToArray(_useState7, 2),
      header = _useState8[0],
      setHeader = _useState8[1];

  var _useState9 = useState({}),
      _useState10 = _slicedToArray(_useState9, 2),
      body = _useState10[0],
      setBody = _useState10[1];

  var _useState11 = useState(''),
      _useState12 = _slicedToArray(_useState11, 2),
      name = _useState12[0],
      setName = _useState12[1];

  var _useState13 = useState([]),
      _useState14 = _slicedToArray(_useState13, 2),
      upHeader = _useState14[0],
      setUpHeader = _useState14[1];

  var _useState15 = useState([]),
      _useState16 = _slicedToArray(_useState15, 2),
      upBody = _useState16[0],
      setUpBody = _useState16[1];

  var letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O'];

  var _useState17 = useState(''),
      _useState18 = _slicedToArray(_useState17, 2),
      error = _useState18[0],
      setError = _useState18[1];

  var _useState19 = useState(false),
      _useState20 = _slicedToArray(_useState19, 2),
      showMapData = _useState20[0],
      setshowmapData = _useState20[1];

  useEffect(function () {
    var downloadFile = /*#__PURE__*/function () {
      var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
        var txt, i, j, blob, href, link;
        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                txt = [];

                if (upHeader[0] !== "undefined" && upBody[0] !== "undefined") {
                  txt = '[\n';

                  for (i = 0; i < upBody.length; i++) {
                    console.log(i, 'i');
                    txt += '{\n';

                    for (j = 0; j < upHeader.length; j++) {
                      console.log(j, 'j');

                      if (sugg_mult_header.includes(upHeader[j])) {
                        txt += '"' + upHeader[j] + '": [';

                        while (upHeader[j] === upHeader[j + 1]) {
                          if (upBody[i][j] !== "") {
                            txt += '"' + upBody[i][j] + '"';
                            if (j !== upHeader.length - 1 || upBody[i][j + 1] !== "") txt += ', ';
                            j++;
                          }
                        }

                        if (upBody[i][j] !== "") {
                          txt += '"' + upBody[i][j] + '"]';
                        } else txt += ']';
                      } else if (upBody[i][j] !== "") {
                        txt += '"' + upHeader[j] + '": "' + upBody[i][j] + '"';
                      }

                      if (j !== upHeader.length - 1) txt += ',\n';
                    }

                    txt += '\n}';
                    if (i !== upBody.length - 1) txt += ',\n';
                  }

                  txt += '\n]';
                }

                blob = new Blob([txt], {
                  type: 'application/json'
                });
                _context.next = 5;
                return URL.createObjectURL(blob);

              case 5:
                href = _context.sent;
                link = document.getElementById('a');
                link.href = href;
                link.download = "data.json";
                if (txt.length !== 4) document.getElementById('btn').style.display = 'block';else document.getElementById('btn').style.display = 'none';

              case 10:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }));

      return function downloadFile() {
        return _ref.apply(this, arguments);
      };
    }();

    downloadFile();
  }, [upHeader, upBody, sugg_mult_header]);

  var uploadFile = function uploadFile(headers) {
    var formData = new FormData();
    formData.append('header', JSON.stringify(headers));
    formData.append('body', JSON.stringify(body));
    formData.append('name', name);
    axios.post('http://abed.dev.joylab.ca/csv-react-php/backend/csvData.php', formData, {
      headers: {
        'content-type': 'multipart/form-data'
      }
    }).then(function (response) {
      console.log(response.data, 'yes');
      setUpHeader(response['data']['header']);
      setUpBody(response['data']['body']);
      setHeader([]);
      setshowmapData(false);
    });
  };

  var getData = function getData(e) {
    if (e == null) {
      setRowdata([]);
      setshowmapData(false);
      setBody({});
    } else {
      var formData = new FormData();
      formData.append('avatar', e);
      axios.post('http://hussein.dev.joylab.ca/csv-react-php/backend/csvData.php', formData, {
        headers: {
          'content-type': 'multipart/form-data'
        }
      }).then(function (response) {
        console.log(response);

        if (response['data']['error'] != '') {
          setError(response['data']['error']);
          document.querySelector("#root > div > div.content > div.container > div.file-display-container > div > div.file-remove").click();
        } else {
          setError('');
          setSuggHeader([]);
          setRowdata(response['data']['output']);
          setBody(response['data']['body']);
          setshowmapData(true);
          setName(response['data']['name']);
          setSuggHeader(response['data']['sugg-headers']);
          setUpHeader([]);
          setUpBody([]);
          setHeader([]);

          if (response['data']['sugg-headers'] == 'false') {
            setSuggMultHeader(['']);
          } else {
            setSuggMultHeader(response['data']['sugg-mult-headers']);
          }
        }
      });
    }
  };

  var changeOption = function changeOption(event, index) {
    var value = event.target.value;

    var headers = _toConsumableArray(header);

    document.getElementById('error' + index).style.display = 'none';
    headers[index] = value;
    setHeader(headers);
    removeOption(headers);

    if (headers.length === rowdata.length) {
      for (var i = 0; i < headers.length; i++) {
        if (headers[i] === undefined || headers[i] === "0") {
          return;
        }
      }

      uploadFile(headers);
    }
  };

  var goBack = function goBack() {
    if (rowdata.length > 0) {
      setshowmapData(true);
    }

    setUpHeader([]);
    setUpBody([]);
  };

  var removeOption = function removeOption(headers) {
    for (var j = 0; j < sugg_header.length; ++j) {
      if (headers.includes(sugg_header[j]) && !sugg_mult_header.includes(sugg_header[j])) {
        var i = sugg_header.indexOf(sugg_header[j]);
        var list, index;
        list = document.getElementsByClassName('option' + i);

        for (index = 0; index < list.length; ++index) {
          list[index].style.display = 'none';
        }
      } else {
        var i = sugg_header.indexOf(sugg_header[j]);
        var list, index;
        list = document.getElementsByClassName('option' + i);

        for (index = 0; index < list.length; ++index) {
          list[index].style.display = 'block';
        }
      }
    }
  };

  return /*#__PURE__*/React.createElement("div", {
    className: "container py-3"
  }, /*#__PURE__*/React.createElement("div", {
    className: "text-center row"
  }, /*#__PURE__*/React.createElement("h1", {
    className: "col-md-8 offset-md-2"
  }, "CSV File Uploader"), /*#__PURE__*/React.createElement("div", {
    className: "col-md-2 mt-3 d-flex justify-content-around",
    style: {
      float: 'right'
    }
  }, /*#__PURE__*/React.createElement("a", {
    title: "CSV file",
    href: "http://hussein.dev.joylab.ca/csv-react-php/backend/uploads/CSV Sample.csv",
    download: true
  }, " ", /*#__PURE__*/React.createElement("i", {
    className: "fas fa-file-csv"
  })), /*#__PURE__*/React.createElement("a", {
    title: "EXCEL file",
    href: "http://hussein.dev.joylab.ca/csv-react-php/backend/uploads/Excel sample.xlsx",
    download: true
  }, /*#__PURE__*/React.createElement("i", {
    className: "fas fa-file-excel"
  }), " "), /*#__PURE__*/React.createElement("a", {
    title: "Empty file",
    href: "http://hussein.dev.joylab.ca/csv-react-php/backend/uploads/Empty Sample.csv",
    download: true
  }, /*#__PURE__*/React.createElement("i", {
    className: "fas fa-file"
  }), " "))), /*#__PURE__*/React.createElement("div", {
    className: "content step col-md-11"
  }, /*#__PURE__*/React.createElement("div", {
    className: "step_div row mx-0"
  }, /*#__PURE__*/React.createElement("h3", null, "Step 1: Upload your CSV, Excel")), /*#__PURE__*/React.createElement(Dropzone, {
    ref: DropzoneRef,
    getData: getData
  })), error && /*#__PURE__*/React.createElement("div", {
    className: "w-100 text-center"
  }, /*#__PURE__*/React.createElement("div", {
    className: "col-md-8 mx-auto "
  }, /*#__PURE__*/React.createElement("div", {
    role: "alert",
    className: "fade alert alert-danger alert-dismissible show"
  }, /*#__PURE__*/React.createElement("button", {
    type: "button",
    className: "close",
    onClick: function onClick() {
      return setError('');
    }
  }, /*#__PURE__*/React.createElement("span", {
    "aria-hidden": "true"
  }, "x")), /*#__PURE__*/React.createElement("div", {
    className: "alert-heading h4"
  }, "Error!"), /*#__PURE__*/React.createElement("p", null, "Incorrect number of columns, you can download our sample CSV file for more info.")))), /*#__PURE__*/React.createElement("div", {
    className: "mx-auto my-5 col-md-11"
  }, showMapData && /*#__PURE__*/React.createElement("div", {
    className: "step"
  }, /*#__PURE__*/React.createElement("div", {
    className: "step_div row m-0"
  }, /*#__PURE__*/React.createElement("h3", null, "Step 2: Map your fields ")), /*#__PURE__*/React.createElement("div", {
    className: "text-center col-md-8 mx-auto"
  }, /*#__PURE__*/React.createElement("div", {
    className: "row  py-2",
    style: {
      textAlign: 'left',
      marginLeft: '50px'
    }
  }, /*#__PURE__*/React.createElement("div", {
    className: " col-md-6"
  }, /*#__PURE__*/React.createElement("h5", null, " Uploaded File Fields")), /*#__PURE__*/React.createElement("div", {
    className: "col-md-5"
  }, /*#__PURE__*/React.createElement("h5", null, " Map Fields"))), rowdata.map(function (item, index) {
    return /*#__PURE__*/React.createElement("div", {
      key: index,
      className: "row border mx-0"
    }, /*#__PURE__*/React.createElement("div", {
      className: "col-md-7 row ",
      style: {
        textAlign: 'start'
      }
    }, /*#__PURE__*/React.createElement("div", {
      className: "col-md-2 border-right py-2 px-4",
      style: {
        background: '#eee'
      }
    }, " ", letters[index], " "), /*#__PURE__*/React.createElement("p", {
      className: "text-dark m-0 py-2 col-md-10"
    }, " ", item, " ", /*#__PURE__*/React.createElement("span", {
      style: {
        float: 'right'
      }
    }, /*#__PURE__*/React.createElement("i", {
      className: "fa fa-arrow-right",
      "aria-hidden": "true"
    })))), /*#__PURE__*/React.createElement("div", {
      className: "col-md-5 py-2",
      style: {
        textAlign: 'start'
      }
    }, sugg_header ? /*#__PURE__*/React.createElement("select", {
      className: "custom-select",
      onChange: function onChange(e) {
        return changeOption(e, index);
      },
      defaultValue: "0",
      id: 'select' + index
    }, /*#__PURE__*/React.createElement("option", {
      value: "0"
    }, "Map to"), sugg_header.map(function (header, i) {
      return /*#__PURE__*/React.createElement("option", {
        key: i,
        value: header,
        className: 'option' + i
      }, header);
    })) : /*#__PURE__*/React.createElement("select", {
      defaultValue: "0",
      className: "custom-select",
      onChange: function onChange(e) {
        return changeOption(e, index);
      },
      id: 'select' + index
    }, /*#__PURE__*/React.createElement("option", {
      value: "0"
    }, "Map to")), /*#__PURE__*/React.createElement("p", {
      className: "error m-0 px-2",
      style: {
        fontSize: 13,
        display: 'none'
      },
      id: 'error' + index
    }, "Address choosed twice ")));
  })))), upHeader ? /*#__PURE__*/React.createElement("div", {
    className: "col-md-11 mx-auto"
  }, /*#__PURE__*/React.createElement("br", null), /*#__PURE__*/React.createElement("table", {
    className: "table  table-striped"
  }, /*#__PURE__*/React.createElement("tbody", null, /*#__PURE__*/React.createElement("tr", null, upHeader.map(function (header, i) {
    return /*#__PURE__*/React.createElement("th", {
      key: i
    }, header);
  })), upBody.slice(0, 4).map(function (data, j) {
    return /*#__PURE__*/React.createElement("tr", {
      key: j
    }, data.map(function (cell, k) {
      return /*#__PURE__*/React.createElement("td", {
        key: k
      }, cell);
    }));
  }))), /*#__PURE__*/React.createElement("div", {
    id: "btn",
    className: " row mx-0 my-4",
    style: {
      display: 'none'
    }
  }, /*#__PURE__*/React.createElement("button", {
    className: "col-md-3 btn btn-primary",
    style: {
      float: 'right'
    }
  }, /*#__PURE__*/React.createElement("a", {
    id: "a",
    href: "http://joylab.ca",
    className: "text-white",
    style: {
      textDecoration: "none"
    }
  }, "Download json data")), /*#__PURE__*/React.createElement("button", {
    onClick: goBack,
    className: "col-md-2 btn btn-primary",
    style: {
      float: 'left'
    }
  }, "GO Back"))) : /*#__PURE__*/React.createElement("div", null));
}

export { FileUploadForm };
