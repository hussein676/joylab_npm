import React, { useRef, useState, useEffect} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import Dropzone from './Dropzone';
import axios from 'axios';
import './fontawesome/css/all.css';


function FileUploadForm() {  
    const DropzoneRef = useRef();
    const [rowdata, setRowdata] = useState([]);
    const [sugg_header, setSuggHeader] = useState([]);
    const [sugg_mult_header, setSuggMultHeader] = useState([]);
    const [header, setHeader] = useState([]);        
    const [body, setBody] = useState({});
    const [name, setName] = useState('');
    const [upHeader, setUpHeader] = useState([]);
    const [upBody, setUpBody] = useState([]);
    const letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O'];
    const [error, setError] = useState('');
    const [showMapData, setshowmapData] = useState(false);
    
    useEffect(()=>{ 
    	const downloadFile = async () => {
    		var txt = [];
	    	if (upHeader[0] !=="undefined" && upBody[0] !=="undefined") {
          txt = '[\n';
	    		for (var i = 0; i < upBody.length; i++) {
            
	    		console.log(i,'i')
	    			txt += '{\n';
	    			for (var j = 0; j < upHeader.length; j++) {
	    				console.log(j,'j')
              if (sugg_mult_header.includes(upHeader[j])) {
	    					txt += '"' + upHeader[j] + '": [';
	    					while (upHeader[j] === upHeader[j+1]) {
	    						if (upBody[i][j] !== "") {
		    						txt += '"' + upBody[i][j] + '"';
		    						
		    						if ((j!==upHeader.length-1) || (upBody[i][j+1] !== ""))
  									txt += ', ';
		    						j++;
		    						}
		    				}
		    				if (upBody[i][j] !== "") {
	    						txt += '"' + upBody[i][j] + '"]';
	    					}
	    					else txt += ']';   					
	    				}
	    				else if (upBody[i][j] !== "") {
	    					txt += '"' + upHeader[j] + '": "' + upBody[i][j] + '"';
	    					}
  					
  					if (j!==upHeader.length-1)
  						txt += ',\n';
  				}
				txt += '\n}';
				if (i!==upBody.length-1)
  						txt += ',\n';
			}
			txt += '\n]';
	    	}
	    	
		  const blob = new Blob([txt],{type:'application/json'});
		  const href = await URL.createObjectURL(blob);
		  const link = document.getElementById('a');
		  link.href = href;
		  link.download = "data.json";
		  if (txt.length !== 4)
		  	document.getElementById('btn').style.display='block';
		  else document.getElementById('btn').style.display='none';
	   	}
	    
    	downloadFile();
    },[upHeader, upBody, sugg_mult_header]);

    const uploadFile=(headers)=> 
    {
        var formData = new FormData();        
            formData.append('header',JSON.stringify(headers));
            formData.append('body',JSON.stringify(body));
            formData.append('name',name);

        axios.post('http://abed.dev.joylab.ca/csv-react-php/backend/csvData.php', formData,
            {
                headers: 
                {
                'content-type': 'multipart/form-data',
                }
            }).then(response => {
                 console.log(response.data,'yes');
                 setUpHeader(response['data']['header']);
                 setUpBody(response['data']['body']);
                 setHeader([]);
                 setshowmapData(false);
               })
      }

    const getData= (e)=> 
    {
        if(e == null)
        {
        setRowdata([]);
        setshowmapData(false)
        setBody({});
        }
        else 
        {           
            var formData = new FormData();        
            formData.append('avatar',e);

            axios.post('http://hussein.dev.joylab.ca/csv-react-php/backend/csvData.php', formData,{
                headers: {
                'content-type': 'multipart/form-data',
                }
            }).then(response => {
                console.log(response);
                if(response['data']['error'] != ''){
                  setError(response['data']['error']) 
                  document.querySelector("#root > div > div.content > div.container > div.file-display-container > div > div.file-remove").click();
               }
                else{
                  setError('');
                  setSuggHeader([]);
                  setRowdata(response['data']['output']);
                  setBody(response['data']['body']); 
                  setshowmapData(true);
                  setName(response['data']['name']);
                  setSuggHeader(response['data']['sugg-headers']);
                  setUpHeader([]);
                  setUpBody([]);
                  setHeader([]);
                  if (response['data']['sugg-headers'] == 'false') {
                    setSuggMultHeader(['']); 
                  }
                  else {
                    setSuggMultHeader(response['data']['sugg-mult-headers']);
                  }
                  }
            })
        }
    }
    const changeOption=(event,index)=> 
    {    
        var value = event.target.value;
        var headers = [...header];
        document.getElementById('error'+index).style.display='none';
        headers[index] = value;
        setHeader(headers);
        removeOption(headers)
        if(headers.length === rowdata.length)
         {
            for(var i = 0;i<headers.length;i++)
              {
                  if(headers[i] === undefined || headers[i] === "0")
                  {

                     return;
                  }            
              }
              uploadFile(headers);

          }
      

  }
  const goBack = () => {
    if(rowdata.length>0){

      setshowmapData(true);
      }
      setUpHeader([]);
       setUpBody([]);

  }
  const removeOption=(headers)=> {
    for(var j = 0;j < sugg_header.length; ++j){
      if (headers.includes(sugg_header[j]) && !sugg_mult_header.includes(sugg_header[j])) {
        var i = sugg_header.indexOf(sugg_header[j])
        var list, index;
        list = document.getElementsByClassName('option'+i);
        for (index = 0; index < list.length; ++index) {
            list[index].style.display='none';
        }
    }
    else{
    var i = sugg_header.indexOf(sugg_header[j])
         var list, index;
         list = document.getElementsByClassName('option'+i);
         for (index = 0; index < list.length; ++index) {
             list[index].style.display='block';
         }

    }
  }

}
  return (
      <div className='container py-3' >
        <div className='text-center row'  >
          <h1 className='col-md-8 offset-md-2'>CSV File Uploader</h1>
        <div className='col-md-2 mt-3 d-flex justify-content-around' style={{float:'right'}}>
       <a title = 'CSV file' href = 'http://hussein.dev.joylab.ca/csv-react-php/backend/uploads/CSV Sample.csv'  download> <i className="fas fa-file-csv"></i></a>
       <a title='EXCEL file' href = 'http://hussein.dev.joylab.ca/csv-react-php/backend/uploads/Excel sample.xlsx'  download><i className="fas fa-file-excel"></i> </a>
       <a title = 'Empty file'href = 'http://hussein.dev.joylab.ca/csv-react-php/backend/uploads/Empty Sample.csv'  download><i className="fas fa-file"></i> </a> 

       </div>
        </div>
        <div className = 'content step col-md-11'>
          <div className="step_div row mx-0" >
            <h3>Step 1: Upload your CSV, Excel</h3>
            </div>
            <Dropzone ref={DropzoneRef}  getData={getData}/>
          </div>
       {error&&
          <div className='w-100 text-center'>
          <div className='col-md-8 mx-auto '><div role='alert' className='fade alert alert-danger alert-dismissible show'>
           <button type='button' className='close' onClick={() => setError('')}>
           <span aria-hidden='true'>x</span>
           </button>
           <div className='alert-heading h4'>Error!</div>
           <p>Incorrect number of columns, you can download our sample CSV file for more info.</p></div></div>
           </div>
       }      
      <div className='mx-auto my-5 col-md-11' >
        {showMapData &&
       <div className = 'step'> 
        <div className='step_div row m-0' >
             <h3>Step 2: Map your fields </h3>
             </div>
        <div className='text-center col-md-8 mx-auto'>
       <div className='row  py-2' style={{textAlign:'left', marginLeft:'50px' }}>
          <div className=' col-md-6' >
         <h5> Uploaded File Fields</h5>
          </div>
          <div className='col-md-5' >
          <h5> Map Fields</h5>
          </div>
       </div>    
       {rowdata.map((item,index) =>
         <div key={index} className='row border mx-0' >
            
            <div className='col-md-7 row ' style={{textAlign: 'start'}}>
            <div className='col-md-2 border-right py-2 px-4' style={{background:'#eee'}}> {letters[index]} </div>
            <p  className='text-dark m-0 py-2 col-md-10'> {item} <span style={{float:'right'}}><i className="fa fa-arrow-right" aria-hidden="true"></i></span></p>
            </div>
            <div className='col-md-5 py-2' style={{textAlign:'start'}} >

                {sugg_header
                    ?
                    <select className='custom-select' onChange={e => changeOption(e, index)} defaultValue="0"  id={'select' + index}>
                        <option  value='0'>Map to</option>
                        {sugg_header.map((header, i) =>
                            <option key={i} value={header} className={'option'+i} >{header}</option>
                        )
                        }
                    </select>
                    :
                    <select defaultValue='0' className='custom-select' onChange={e => changeOption(e, index)} id={'select' + index}>
                        <option value='0'>Map to</option>
                        
                    </select>
                }
                <p className="error m-0 px-2"style={{fontSize:13 ,display:'none'}} id={'error' + index} >Address choosed twice </p>
            </div>
           
            </div>
        )}
         </div>
        </div>
         
          }
  
        </div>

	    {upHeader
                    ?
                    <div className = 'col-md-11 mx-auto'>
                    <br/>
                	<table className = "table  table-striped">
                    		<tbody>
                    		    <tr>
                    			{upHeader.map((header, i) =>
                            			<th key={i}>{header}</th>
                        		)
                        		}
                    		     </tr>
                    		     
                    		     {upBody.slice(0,4).map((data, j) =>
                                  <tr key={j}>
                                    
                                        {data.map((cell, k) =>
                                      <td key={k}>{cell}</td>
                                )}
                                    
                                    </tr>
                            )}
                    		     
                    		</tbody>
                    	</table>
                      <div id='btn' className=" row mx-0 my-4" style={{display:'none'}}>
                      <button  className="col-md-3 btn btn-primary"  style={{float: 'right'}} >
                       <a id="a" href="http://joylab.ca" className="text-white" style={{textDecoration:"none"}}>Download json data</a>
                       </button>
                       
                      <button onClick={goBack} className="col-md-2 btn btn-primary"  style={{float: 'left'}} >
                       GO Back
                       </button>
                       </div>
                    </div>
                    :
                    <div/>
            }
        </div>
       )    
}

export default FileUploadForm;
