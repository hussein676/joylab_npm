import babel from '@rollup/plugin-babel';
import external from 'rollup-plugin-peer-deps-external';
import del from 'rollup-plugin-delete';

import css from 'rollup-plugin-css-only'

export default {
    input: "src/index.js",
    output: [
        { file: "dist/index.cjs.js", format: 'cjs' },
        { file: "dist/index.esm.js", format: 'esm' }
    ],
    plugins: [
        external(),
        css(["src/components/Dropzone.css"]),
        babel({
            exclude: 'node_modules/**'
        }),
        del({ targets: ['dist/*'] }),
    ],
    external: Object.keys({
        "react-dom": "^17.0.1",
        "react": "^17.0.1" ,
        "axios": "^0.21.1",
        "bootstrap": "^4.6.0"

    } || {},),
};